#include "Noborimashou.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <filesystem>
#include <thread>
#include "direct.h"

Noborimashou::Noborimashou(QWidget *parent)
	: QMainWindow(parent), mUI(&mStockData)
{
	try {
		/*std::thread loadData([this]() { 
			mStockData.loadData(); 
		});*/
		mStockData.loadData();
		mUI.setupUI(this);
		//loadData.join();
	}
	catch (const std::exception& e) {
		auto msgBox = new QMessageBox(QMessageBox::Information, "Error", QString::fromStdString(e.what()));
		msgBox->open();
	}
}

