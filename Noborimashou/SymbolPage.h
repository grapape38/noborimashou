#pragma once

#include "NoboWidgets.h"
#include "NoboHTTP.h"

class Noborimashou_UI;

class SymbolPageView : public QScrollArea {
	Q_OBJECT
public:
	SymbolPageView(int symbolID, StockUserData* data);
public slots:
	void quotesLoaded(const QuoteList& ql);
private:
	void setupUI();
	void connectUISignals();
	void quotesGrid();
	void quotesLinePlot();
	void quotesCalc();
	void loadQuotes();
private:
	StockDataHTTP mHandle;
	QString mName;
	int mID;
	QuoteList mQuotes;
	QLabel* mLoadingLabel;
	QVBoxLayout* mLayout;
	QHBoxLayout* mCalc;
	QGridLayout* mQuoteGrid;
	QWidget* mStockBox;
};

