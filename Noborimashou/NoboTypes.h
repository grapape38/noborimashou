#pragma once

#include <QDateTime>
#include <nlohmann/json.hpp>

#include <utility>

using json = nlohmann::json;

struct Quote {
	float open;
	float high;
	float low;
	float close;
	float volume;
	QDateTime date;
};

using QuoteList = std::vector<Quote>;

inline void from_json(const json& j, Quote& q) {
	const auto qdate = QString::fromStdString(j["date"]);
	const auto qtime = QString::fromStdString(j["minute"]);
	QString dateTime = QString("%1T%2:00").arg(qdate, qtime);
	q = {
		j["open"], j["high"], j["low"], j["close"], j["volume"],
		QDateTime::fromString(dateTime, Qt::ISODate)
	};
}

inline void to_json(json& j, const Quote& q) {
	auto date = q.date.date().toString(Qt::ISODate).toStdString();
	auto minute = q.date.time().toString(Qt::ISODate).toStdString();
	j = {
		{"open", q.open}, {"high", q.high}, {"low", q.low},
		{"close", q.close}, {"volume", q.volume}, {"date", date},
		{"minute", minute} };
}
