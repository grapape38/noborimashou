#include "Noborimashou_UI.h"
#include "NoboCalc.h"

void Noborimashou_UI::setupUI(QMainWindow* window) {
    if (window->objectName().isEmpty())
        window->setObjectName(QStringLiteral("NoborimashouClass"));
    window->resize(800, 600);

    mTabWidget = new QTabWidget(window);
    makeTabs(window);
    //makeMenuAndStatusBars(window);

    window->setCentralWidget(mTabWidget);
    window->setWindowTitle("Noborimashou Trading");
    QMetaObject::connectSlotsByName(window);
}


void Noborimashou_UI::makeMenuAndStatusBars(QMainWindow* window)
{
    auto menuBar = new QMenuBar(window);
    menuBar->setObjectName(QStringLiteral("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 643, 26));
    window->setMenuBar(menuBar);
    auto mainToolBar = new QToolBar(window);
    mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
    window->addToolBar(Qt::TopToolBarArea, mainToolBar);
    auto statusBar = new QStatusBar(window);
    statusBar->setObjectName(QStringLiteral("statusBar"));
    window->setStatusBar(statusBar);
}

void Noborimashou_UI::makeTabs(QMainWindow* window) {
    mTabWidget = new QTabWidget(window);
    mTabWidget->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    mTabWidget->setUsesScrollButtons(false);
    mTabWidget->setTabsClosable(true);

    const auto closeRequestedCB = [this](int index) {
        auto w = mTabWidget->widget(index);
        mTabWidget->removeTab(index);
        delete w;
    };
    connect(mTabWidget, &QTabWidget::tabCloseRequested, closeRequestedCB);

    mTabWidget->addTab(new SplashPage(this), "Dashboard");
    mTabWidget->addTab(new SymbolsView(this), "Symbols Browser");

    window->setCentralWidget(mTabWidget);
}

void Noborimashou_UI::viewSymbol(int symbolID)
{
    const auto symName = QString::fromStdString(mData->symName(symbolID));
    try {
        auto idx = mTabWidget->addTab(new SymbolPageView(symbolID, stockUserData()), symName);
        mTabWidget->setCurrentIndex(idx);
    }
    catch (std::exception& e) {
        auto msgBox = new QMessageBox(QMessageBox::Information, "Error", e.what());
        msgBox->open();
    }
}

SymbolsView::SymbolsView(Noborimashou_UI* ui)
    : QScrollArea(), mUI(ui)
{
    setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    mSymbolsList = new SymbolsBrowserList(ui->stockUserData());

    for (const auto& info : ui->stockUserData()->allSymbolInfo()) {
        mSymbolsList->addSymbol(info.symbolID);
    }
    setWidget(mSymbolsList);
    connectUISignals();
}

void SymbolsView::connectUISignals()
{
    connect(mSymbolsList, &SymbolsList::symbolPressed, mUI, &Noborimashou_UI::viewSymbol);
}

HoldingsBox::HoldingsBox(Noborimashou_UI* ui, QWidget* parent)
    : QGroupBox("Holdings", parent), mUI(ui)
{
    const auto data = ui->stockUserData();
    mSymbolsList = new SymbolsHoldingList(data, this);

    for (auto holding : data->holdings()) {
        mSymbolsList->addSymbol(data->getSymbolID(holding->symbol));
    }
    connectUISignals();
}

WatchBox::WatchBox(Noborimashou_UI* ui, QWidget* parent)
    : QGroupBox("Watch List", parent), mUI(ui)
{
    const auto data = ui->stockUserData();
    mSymbolsList = new SymbolsWatchList(data, this);
    for (const auto& symbolID : data->watchList()) {
        mSymbolsList->addSymbol(symbolID);
    }
    connectUISignals();
}

SplashPage::SplashPage(Noborimashou_UI* ui)
{
    mHoldings = new HoldingsBox(ui, this);
    mWatch = new WatchBox(ui, this);
    auto hLayout = new QHBoxLayout(this);
    hLayout->addWidget(mHoldings);
    hLayout->addWidget(mWatch);
}

void HoldingsBox::connectUISignals()
{
    connect(mSymbolsList, &SymbolsList::symbolPressed, mUI, &Noborimashou_UI::viewSymbol);
}

void WatchBox::connectUISignals()
{
    connect(mSymbolsList, &SymbolsList::symbolPressed, mUI, &Noborimashou_UI::viewSymbol);
}