#include "StockData.h"
#include <QtWidgets/QMessageBox>

void StockUserData::loadData()
{
	loadSymbols();
	StockDataSQL handle;
	auto holdings = handle.getHoldings();
	const auto watchNames = handle.getWatchList();
	int symbolID = 0;
	for (const auto& name : mSymbolNames) {
		mSymbolUserInfo.emplace_back(symbolID);
		mNameToID[name] = symbolID++;
	}
	for (auto& holding : holdings) {
		symbolID = mNameToID.at(holding.symbol);
		mSymbolUserInfo[symbolID].holding = std::move(holding);
	}
	for (const auto& symbol : watchNames) {
		symbolID = mNameToID.at(symbol);
		mWatchList.push_back(symbolID);
		mSymbolUserInfo[symbolID].isWatched = true;
	}
}

void StockUserData::saveToDB()
{
	StockDataSQL handle;
	for (const auto& holding : holdings()) {
		handle.addHolding(*holding);
	}
	for (const auto& symID : mWatchList) {
		handle.addWatch(symName(symID));
	}
}

void StockUserData::writeSymbolsToDB()
{
	sqlite::database db("Noborimashou.db");
	db <<
		"create table if not exists symbols("
		"	symbol string primary key"
		");";
	for (const auto& symbol : mSymbolNames) {
		db << "insert into symbols (symbol) values (?);"
			<< symbol;
	}
	int count = 0;
	db << "select count(*) from symbols" >> count;
	if (count != (int)mSymbolNames.size()) {
		throw std::runtime_error("Number of symbols does not match in database");
	}
	else {
		auto msgBox = new QMessageBox(QMessageBox::Information, "Success", "Successfully inserted symbol names");
		msgBox->open();
	}
}

void StockUserData::loadSymbols()
{
	char dataDirOwner[100];
	char const* dataDir = nullptr;// std::getenv("DATA_DIR");
	if (!dataDir) {
		dataDir = getcwd(dataDirOwner, 100);
		//throw std::runtime_error("Data Directory env variable not set");
	}
	std::filesystem::path dataPath(dataDir);
	dataPath /= "tickers.txt";
	std::ifstream tickersFile(dataPath, std::ifstream::in);
	if (!tickersFile.is_open()) {
		throw std::runtime_error("Could not open tickers file");
	}
	mSymbolNames.reserve(1000);
	std::unordered_set<std::string> uniqueTickers;
	std::string tickerName;
	while (tickersFile >> tickerName) {
		if (uniqueTickers.find(tickerName) == uniqueTickers.end()) {
			uniqueTickers.insert(tickerName);
			mSymbolNames.push_back(tickerName);
		}
	}
}
