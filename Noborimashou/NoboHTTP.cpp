#include "NoboHTTP.h"
#include <filesystem>
#include <fstream>
#include <iostream>
#include <functional>

namespace {
	static const QString API_KEY = "Tpk_5662078a0b824a7a88aab40b6376c7b5";
	static const QString URL_BASE = "https://sandbox.iexapis.com/stable";
	inline void loadAPIKeys()
	{
		if (!API_KEY.isEmpty()) return;
		std::filesystem::path dataPath("secrets");
		dataPath /= "api_keys.txt";
		std::ifstream secretsFile(dataPath, std::ifstream::in);
		if (!secretsFile.is_open()) {
			throw std::runtime_error("Could not open file with api key");
		}
		std::string api_key;
		secretsFile >> api_key;
		if (api_key.empty()) {
			throw std::runtime_error("Error: empty api key");
		}
		//API_KEY = QString::fromStdString(api_key);
	}
}

StockDataHTTP::StockDataHTTP() : mManager(this)
{
	//loadAPIKeys();
}

void StockDataHTTP::getHistoricalQuotes(const QString& /*symbol*/, std::tm from, std::tm /*to*/)
{
	//unimplemented
}

void StockDataHTTP::getHistoricalLastMonth(const QString& symbol)
{
	QString url;
	QTextStream ts(&url);
	ts << URL_BASE << "/stock/" << symbol << "/chart/1mm" << "?token=" << API_KEY;
	makeGetRequest(url, RequestTypes::HistoricalLastMonth);
}

void StockDataHTTP::makeGetRequest(const QString& url, RequestTypes ty)
{
	QNetworkRequest request;
	request.setUrl(QUrl(url));

	QNetworkReply* reply = mManager.get(request);
	const auto handleRes = [=]() {
		handleResults(reply, ty);
	};
	connect(reply, &QNetworkReply::finished, handleRes);
}

void StockDataHTTP::handleResults(QNetworkReply* reply, RequestTypes ty)
{
	if (reply->error() != QNetworkReply::NoError) {
		emit requestError(reply->error(), reply->errorString());
		return;
	}
	auto jfut = QtConcurrent::run([=]() {
		auto bytes = reply->readAll();
		return json::parse(bytes.data());
	});
	auto futureWatcher = new QFutureWatcher<json>();
	futureWatcher->setFuture(jfut);
	const auto handleJSON = [=]() {
		const auto& j = futureWatcher->result();
		switch (ty) {
		case RequestTypes::HistoricalLastMonth:
		{
			const auto ql = j.get<QuoteList>();
			emit historicalQuotesLoaded(ql);
			break;
		}
		}
		delete futureWatcher;
		reply->deleteLater();
	};
	connect(futureWatcher, &QFutureWatcher<json>::finished, handleJSON);
	
}
