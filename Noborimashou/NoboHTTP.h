#pragma once

#include <string>
#include <vector>
#include <chrono>
#include <memory>
#include <queue>
#include <variant>
#include <unordered_set>
#include <functional>
#include <variant>
#include "qobject.h"
#include "qthread.h"
#include "qmetaobject.h"
#include "qthreadpool.h"
#include "QtConcurrent/qtconcurrentrun.h"
#include "qfuturewatcher.h"
#include "QtNetwork/qnetworkaccessmanager.h"
#include "QtNetwork/qnetworkrequest.h"
#include "QtNetwork/qnetworkreply.h"
#include <sstream>

#include "NoboTypes.h"

enum class RequestTypes {
	HistoricalLastMonth
};

class StockDataHTTP : public QObject {
	Q_OBJECT;
public:
	StockDataHTTP();
	void getHistoricalQuotes(const QString& symbol, std::tm fromDate, std::tm toDate);
	void getHistoricalLastMonth(const QString& symbol);
private:
	void makeGetRequest(const QString& url, RequestTypes ty);
private:
	std::string mAPIKey;
	QNetworkAccessManager mManager;
	void handleResults(QNetworkReply* reply, RequestTypes ty);
signals:
	void requestError(int statusCode, const QString& errMsg);
	void historicalQuotesLoaded(const QuoteList&);
};
