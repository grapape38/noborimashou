#define PYTHON 0

#if PYTHON
    #include "NoboPy.h"
#endif
#include "NoboCalc.h"
#include "SymbolPage.h"
#include "qmdiarea.h"
#include "QtQuick/qquickview.h"

SymbolPageView::SymbolPageView(int symbolID, StockUserData* userData)
    : QScrollArea(), mID(symbolID)
{
    mName = QString::fromStdString(userData->symName(symbolID));
    setupUI();
    connectUISignals();
    loadQuotes();
}

void SymbolPageView::quotesLoaded(const QuoteList& q) {
    mQuotes = q;

    mLayout->removeWidget(mLoadingLabel);
    delete mLoadingLabel;

    quotesLinePlot();
    quotesCalc();
    quotesGrid();
}

void SymbolPageView::quotesGrid()
{
    const auto& q = mQuotes;
    mLayout->addLayout(mQuoteGrid = new QGridLayout);
    /*auto frameLabel = [](const QString& s) {
        auto label = new QLabel(s);
        label->setFrameStyle(QFrame::Box);
        label->setLineWidth(2);
        label->setContentsMargins(2, 2, 2, 2);
        return label;
    };*/
    int j = 0;

    mQuoteGrid->addWidget(new QLabel("Date"), 0, j++);
    mQuoteGrid->addWidget(new QLabel("Time"), 0, j++);
    mQuoteGrid->addWidget(new QLabel("Open"), 0, j++);
    mQuoteGrid->addWidget(new QLabel("High"), 0, j++);
    mQuoteGrid->addWidget(new QLabel("Low"), 0, j++);
    mQuoteGrid->addWidget(new QLabel("Close"), 0, j++);
    mQuoteGrid->addWidget(new QLabel("Volume"), 0, j++);
    for (int i = 0; i < (int)q.size(); i++) {
        j = 0;
        const auto& quote = q[i];
        mQuoteGrid->addWidget(new QLabel(
            quote.date.date().toString(Qt::ISODate)), i + 1, j++);
        mQuoteGrid->addWidget(new QLabel(
            quote.date.time().toString()), i + 1, j++);
        mQuoteGrid->addWidget(new QLabel(
            QString::number(quote.open)), i + 1, j++);
        mQuoteGrid->addWidget(new QLabel(
            QString::number(quote.high)), i + 1, j++);
        mQuoteGrid->addWidget(new QLabel(
            QString::number(quote.low)), i + 1, j++);
        mQuoteGrid->addWidget(new QLabel(
            QString::number(quote.close)), i + 1, j++);
        mQuoteGrid->addWidget(new QLabel(
            QString::number(quote.volume)), i + 1, j++);
    }
}

void SymbolPageView::quotesLinePlot()
{
    const auto& q = mQuotes;
    mStockBox = new QGroupBox(mName + " chart");
    auto stockLayout = new QHBoxLayout(mStockBox);
    auto stockView = new QQuickView;
    stockView->setInitialProperties({ { "myClose", q[0].close } });
    stockView->setSource(QUrl::fromLocalFile("stockchart.qml"));
    
    auto container = QWidget::createWindowContainer(stockView);
    container->setMinimumSize(400, 400);
    stockLayout->addWidget(container);

    mLayout->addWidget(mStockBox);
#if PYTHON
    try {
        const auto j = callPythonJSON(q, PythonMethods::processQuotes);
        makeMessageBox("Python returned: ", QString::fromStdString(j.dump()));
    }
    catch (const std::exception& e) {
        makeMessageBox("Error", QString::fromUtf8(e.what()));
    }
#endif
}

void SymbolPageView::quotesCalc()
{
    mLayout->addLayout(mCalc = new QHBoxLayout);
    const auto atrLabel = new QLabel("");
    const auto updateAtrLabel = [=]() {
        const double atr = getATR(mQuotes);
        atrLabel->setText(QString("ATR: %1").arg(QString::number(atr)));
    };
    const auto btn = new QPushButton("Calculate ATR");
    mCalc->addWidget(btn);
    mCalc->addWidget(atrLabel);
    connect(btn, &QPushButton::clicked, updateAtrLabel);
}

void SymbolPageView::setupUI()
{
    setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    //setStyleSheet("QLabel { border: 1px solid black }");
    auto layoutBox = new QWidget;
    setWidget(layoutBox);
    mLayout = new QVBoxLayout(layoutBox);
    mLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);
    auto symbolLabel = new QLabel(mName);
    mLayout->addWidget(symbolLabel);
    mLayout->addWidget(mLoadingLabel = new QLabel("Loading Quotes..."));
}

void SymbolPageView::connectUISignals()
{
    connect(&mHandle, &StockDataHTTP::historicalQuotesLoaded, this, &SymbolPageView::quotesLoaded);
    connect(&mHandle, &StockDataHTTP::requestError, requestError);
}

void SymbolPageView::loadQuotes()
{
    mHandle.getHistoricalLastMonth(mName);
}
