#include "StockDataSQL.h"

StockDataSQL::StockDataSQL() : mDB("Noborimashou.db")
{}

void StockDataSQL::addHolding(const Holding& holding)
{
	mDB <<
		"create table if not exists holdings("
		"	symbol string primary key"
		"   purchasePrice double"
		"   quantity    double"
		");";
	mDB << "insert into holdings (symbol, purchasePrice, quantity) values (?, ?, ?);"
		<< holding.symbol << holding.purchasePrice << holding.quantity;
}

void StockDataSQL::removeHolding(const std::string& symbol)
{
	mDB <<
		"delete from holdings where symbol == ?;" << symbol;
}

void StockDataSQL::addWatch(const std::string& symbol)
{
	mDB <<
		"create table if not exists watchlist("
		"	symbol string primary key"
		");";
	mDB << "insert into watchlist (symbol) values (?);"
		<< symbol;
}

void StockDataSQL::removeWatch(const std::string& symbol)
{
	mDB <<
		"delete from watchlist where symbol == ?;" << symbol;
}

std::vector<Holding> StockDataSQL::getHoldings()
{
	std::vector<Holding> holdings;
	int count = 0;
	mDB <<
		"select count(*) from sqlite_master where type = "
		"	\"table\" and name = \"holdings\"; " >> count;
	if (count > 0) {
		mDB <<
			"select * from holdings;" >>
			[&holdings](std::string symbol, double purchasePrice, int quantity) {
			holdings.push_back({ symbol, purchasePrice, quantity });
		};
	}
	return holdings;
}

std::vector<std::string> StockDataSQL::getWatchList()
{
	std::vector<std::string> watchList;
	int count = 0;
	mDB <<
		"select count(*) from sqlite_master where type = "
		"	\"table\" and name = \"watchlist\"; " >> count;
	if (count > 0) {
		mDB <<
			"select * from watchlist;" >>
			[&watchList](std::string symbol) {
			watchList.push_back(symbol);
		};
	}
	return watchList;
}