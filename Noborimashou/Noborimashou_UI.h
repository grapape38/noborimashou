#pragma once

#include <QtWidgets/QMessageBox>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QGroupBox>

#include "StockData.h"
#include "NoboWidgets.h"
#include "NoboHTTP.h"
#include "SymbolPage.h"

class Noborimashou_UI;

void requestError(int statusCode, const QString& errMsg);

class SymbolsView : public QScrollArea {
public:
	SymbolsView(Noborimashou_UI* ui);
	void connectUISignals();
private:
	Noborimashou_UI* mUI;
	SymbolsList* mSymbolsList{ nullptr };
};

class HoldingsBox : public QGroupBox {
public:
	HoldingsBox(Noborimashou_UI* ui, QWidget* parent = nullptr);
private:
	void connectUISignals();
	Noborimashou_UI* mUI;
	SymbolsList* mSymbolsList{ nullptr };
};

class WatchBox : public QGroupBox {
public:
	WatchBox(Noborimashou_UI* ui, QWidget* parent = nullptr);
private:
	void connectUISignals();
	Noborimashou_UI* mUI;
	SymbolsList* mSymbolsList{ nullptr };
};

class SplashPage : public QWidget {
public:
	SplashPage(Noborimashou_UI* ui);
private:
	HoldingsBox* mHoldings;
	WatchBox* mWatch;
};

class Noborimashou_UI : public QObject {
	Q_OBJECT

public:
	Noborimashou_UI(StockUserData* data) : mData(data) {}
	void setupUI(QMainWindow* window);
	void viewSymbol(int symbolID);
	StockUserData* stockUserData() {
		return mData;
	}
private:
	void makeMenuAndStatusBars(QMainWindow* window);
	void makeTabs(QMainWindow* window);
private:
	QTabWidget* mTabWidget{ nullptr };

	StockUserData* mData;
};
