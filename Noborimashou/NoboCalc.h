#pragma once
#include <vector>
#include "NoboTypes.h"

inline float getTrueRange(const Quote& prevQuote, const Quote& curQuote) {
	return std::max({
			abs(curQuote.high - prevQuote.close), abs(curQuote.low - prevQuote.close),
			abs(curQuote.high - curQuote.low) });
}

inline float getATR(const QuoteList& ql, int nDays = 14) {
	float sma = 0.0;
	for (size_t i = 1; i < nDays; i++) {
		sma += getTrueRange(ql[i], ql[i - 1]);
	}
	float prevAtr = sma / nDays;
	for (size_t i = nDays; i < ql.size(); i++) {
		auto tr = getTrueRange(ql[i], ql[i - 1]);
		prevAtr = (prevAtr * (nDays - 1) + tr) / nDays;
	}
	return prevAtr;
}

inline std::vector<std::vector<double>> getCorrelation(const std::vector<QuoteList>& ql) {

}
