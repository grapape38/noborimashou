#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <filesystem>
#include <optional>
#include <unordered_set>
#include "direct.h"
#include <sqlite_modern_cpp.h>
#include <qobject.h>
#include <qobjectdefs.h>
#include "StockDataSQL.h"
#include "NoboTypes.h"

struct SymbolUserInfo {
	SymbolUserInfo(int id) : symbolID(id) {};
	int symbolID{ 0 };
	std::optional<Holding> holding;
	bool isWatched{ false };
};

class StockUserData : public QObject {
	Q_OBJECT

public:
	enum class DataMessage {
		WatchAdded,
		WatchRemoved,
		HoldingAdded,
		HoldingRemoved,
		HoldingEdit
	};
	const std::vector<std::string>& symbolNames() {
		return mSymbolNames;
	}
	std::vector<Holding const*> holdings() const {
		std::vector<Holding const*> v;
		for (const auto& info : mSymbolUserInfo) {
			if (info.holding)
				v.push_back(&*info.holding);
		}
		return v;
	}
	const std::vector<int>& watchList() const {
		return mWatchList;
	}
	bool isSymbolWatched(int symbolID) const {
		return mSymbolUserInfo[symbolID].isWatched;
	}
	Holding const* getHolding(int symbolID) const {
		return mSymbolUserInfo[symbolID].holding ?
			&*mSymbolUserInfo[symbolID].holding : nullptr;
	}
	void addWatch(int symbolID) {
		if (!isSymbolWatched(symbolID)) {
			mWatchList.push_back(symbolID);
			mSymbolUserInfo[symbolID].isWatched = true;
			emit dataMessage(symbolID, DataMessage::WatchAdded);
		}
	}
	void removeWatch(int symbolID) {
		if (isSymbolWatched(symbolID)) {
			mWatchList.erase(
				std::remove(mWatchList.begin(), mWatchList.end(), symbolID));
			mSymbolUserInfo[symbolID].isWatched = false;
			emit dataMessage(symbolID, DataMessage::WatchRemoved);
		}
	}
	void toggleWatch(int symbolID) {
		if (!isSymbolWatched(symbolID)) {
			addWatch(symbolID);
		}
		else {
			removeWatch(symbolID);
		}
	}
	void addHolding(int symbolID, const Holding* holding = nullptr) {
		if (!getHolding(symbolID)) {
			mSymbolUserInfo[symbolID].holding = holding ? *holding : Holding{ symName(symbolID) };
			emit dataMessage(symbolID, DataMessage::HoldingAdded);
		}
	}
	void removeHolding(int symbolID) {
		if (getHolding(symbolID)) {
			mSymbolUserInfo[symbolID].holding.reset();
			emit dataMessage(symbolID, DataMessage::HoldingRemoved);
		}
	}
	void toggleHolding(int symbolID) {
		const auto& symbolName = symName(symbolID);
		if (!getHolding(symbolID)) {
			addHolding(symbolID);
		}
		else {
			removeHolding(symbolID);
		}
	}
	const SymbolUserInfo& symbolInfo(int symbolID) const {
		return mSymbolUserInfo[symbolID];
	}
	SymbolUserInfo& symbolInfo(int symbolID) {
		return mSymbolUserInfo[symbolID];
	}
	SymbolUserInfo& symbolInfo(const std::string& name) {
		return mSymbolUserInfo[mNameToID.at(name)];
	}
	const std::vector<SymbolUserInfo>& allSymbolInfo() const {
		return mSymbolUserInfo;
	}
	int getSymbolID(const std::string& name) {
		return mNameToID.at(name);
	}
	const std::string& symName(int symbolID) const {
		return mSymbolNames[symbolID];
	}
	void loadData();
	void saveToDB();
	void writeSymbolsToDB();
	void loadSymbols();
signals:
	void dataMessage(int, DataMessage);
private:
	std::vector<int> mWatchList;
	std::vector<SymbolUserInfo> mSymbolUserInfo;
	std::vector<std::string> mSymbolNames;
	std::unordered_map<std::string, int> mNameToID;
};

