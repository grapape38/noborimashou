#pragma once

#include <vector>
#include <string>
#include <sqlite_modern_cpp.h>

struct Holding {
	Holding(std::string sym) : symbol(sym) {}
	Holding(std::string sym, double p, int q) :
		symbol(sym), purchasePrice(p), quantity(q) {}
	std::string symbol;
	double purchasePrice{ 0. };
	int quantity{ 0 };
};

class StockDataSQL {
public:
	StockDataSQL();
	void addHolding(const Holding& holding);
	void removeHolding(const std::string& symbol);

	void addWatch(const std::string& symbol);
	void removeWatch(const std::string& symbol);

	std::vector<Holding> getHoldings();
	std::vector<std::string> getWatchList();
private:
	sqlite::database mDB;
};