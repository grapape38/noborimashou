#pragma once

#include <QtWidgets/QMainWindow>
#include "StockData.h"
#include "Noborimashou_UI.h"

class Noborimashou : public QMainWindow
{
	Q_OBJECT

public:
	Noborimashou(QWidget *parent = Q_NULLPTR);
	
private:
	StockUserData mStockData;
	Noborimashou_UI mUI;
};