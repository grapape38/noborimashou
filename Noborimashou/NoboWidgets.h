#pragma once

#include <QtWidgets/QGridLayout>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QMessageBox>
#include <QLineEdit>
#include "QtQml/qqmlengine.h"
#include "StockData.h"

inline void makeMessageBox(const QString& title, const QString& msg) {
	auto msgBox = new QMessageBox(QMessageBox::Information, title, msg);
	msgBox->open();
}

inline void requestError(int statusCode, const QString& errMsg)
{
	auto msg = errMsg.isEmpty() ? "Empty" : errMsg;
	msg = QString("Error making request.\nStatus code: %1\nMessage: %2").arg(
		QString::number(statusCode), msg);
	auto msgBox = new QMessageBox(QMessageBox::Information, "Error", errMsg);
	msgBox->open();
}

QQmlEngine* getQMLEngine();

class QSymbolLabel : public QLabel {
public:
	QSymbolLabel(const QString& s, QWidget* parent = nullptr) : QLabel(s, parent) {}
	void QSymbolLabel::enterEvent(QEvent*) override
	{
		setStyleSheet("QLabel { color: blue; text-decoration: underline; }");
	}
	void QSymbolLabel::leaveEvent(QEvent*) override
	{
		setStyleSheet("QLabel { color: black; }");
	}
};

struct SymbolRow {
	using DataMessage = StockUserData::DataMessage;
public:
	const SymbolUserInfo* mInfo{ nullptr };
	QSymbolLabel* mSymbolLabel{ nullptr };
	SymbolRow() = default;
	SymbolRow(
		int symbolID,
		StockUserData* stockData,
		QWidget* parent);
	int symbolID() const {
		return mInfo ? mInfo->symbolID : -1;
	}
	QPushButton* makeWatchButton(StockUserData* stockData, QWidget* parent);
	QPushButton* makeHoldingButton(StockUserData* stockData, QWidget* parent);
	virtual void dataChanged(StockUserData::DataMessage msg) = 0;
	virtual std::vector<QWidget*> widgets() = 0;
};

struct SymbolBrowserRow : public SymbolRow {
	QPushButton* mHoldingButton;
	QPushButton* mWatchButton;
	SymbolBrowserRow(
		int symbolID,
		StockUserData* stockData,
		QWidget* parent);
	virtual void dataChanged(StockUserData::DataMessage msg) override;
	virtual std::vector<QWidget*> widgets() override;
};

struct SymbolWatchRow : public SymbolRow {
	QPushButton* mWatchButton;
	SymbolWatchRow(
		int symbolID,
		StockUserData* stockData,
		QWidget* parent);
	virtual void dataChanged(StockUserData::DataMessage msg) override;
	virtual std::vector<QWidget*> widgets() override;
};

struct SymbolHoldingsRow : public SymbolRow {
	QPushButton* mHoldingButton{ nullptr };
	QLabel* mQuantLbl;
	QLabel* mPriceLbl;
	SymbolHoldingsRow(
		int symbolID,
		StockUserData* stockData,
		QWidget* parent);
	virtual void dataChanged(StockUserData::DataMessage msg) override;
	virtual std::vector<QWidget*> widgets() override;
};

class SymbolsList : public QWidget {
	Q_OBJECT
	
public:
	//static constexpr int START_ROW = 1;
	using RowIterator = std::map<QString, std::unique_ptr<SymbolRow>>::iterator;
	using DataMessage = StockUserData::DataMessage;
	SymbolsList(StockUserData* data, QWidget* parent = nullptr);
	QString symName(int symbolID) {
		return QString::fromStdString(mData->symName(symbolID));
	}
	void addSymbol(int symbolID);
	void removeSymbol(int symbolID);
	RowIterator addRow(int symbolID);
	RowIterator findRow(int symbolID) {
		return mSymbolRows.find(symName(symbolID));
	}
	RowIterator rowsEnd() {
		return mSymbolRows.end();
	}
	virtual void mousePressEvent(QMouseEvent*) override;
	virtual void dataMessageHandler(int symbolID, StockUserData::DataMessage msg) = 0;
	void filterSymbols(const QString& text);
protected slots:
	void dataMessageDispatch(int symbolID, StockUserData::DataMessage msg) {
		dataMessageHandler(symbolID, msg);
	}
signals:
	void symbolPressed(int);
protected:
	void connectSignals();
protected:
	StockUserData* mData;
private:
	virtual std::unique_ptr<SymbolRow> makeRow(int symbolID) = 0;
	std::map<QString, std::unique_ptr<SymbolRow>> mSymbolRows;
	QGridLayout* mSymbolsGrid{ nullptr };
	QLineEdit* mFilterText{ nullptr };
};

class SymbolsBrowserList : public SymbolsList {
	Q_OBJECT
public:
	SymbolsBrowserList(StockUserData* data, QWidget* parent = nullptr) : 
		SymbolsList(data, parent) { }
private:
	virtual std::unique_ptr<SymbolRow> makeRow(int symbolID) override;
	virtual void dataMessageHandler(int symbolID, StockUserData::DataMessage msg);
};
class SymbolsWatchList : public SymbolsList {
	Q_OBJECT

public:
	SymbolsWatchList(StockUserData* data, QWidget* parent = nullptr) :
		SymbolsList(data, parent) { }
private:
	virtual std::unique_ptr<SymbolRow> makeRow(int symbolID) override;
	virtual void dataMessageHandler(int symbolID, StockUserData::DataMessage msg);
};

class SymbolsHoldingList : public SymbolsList {
	Q_OBJECT

public:
	SymbolsHoldingList(StockUserData* data, QWidget* parent = nullptr) :
		SymbolsList(data, parent) { }
private:
	virtual std::unique_ptr<SymbolRow> makeRow(int symbolID) override;
	virtual void dataMessageHandler(int symbolID, StockUserData::DataMessage msg);
};

class HoldingsDialog : public QDialog {
	Holding mHolding;
	QLineEdit* mQuantityEdit;
	QLineEdit* mPriceEdit;
	//QHBoxLayout* mLayout;
public:
	HoldingsDialog(const std::string& symName, QWidget* parent = nullptr);
	const Holding& getHolding() const { return mHolding; }
};