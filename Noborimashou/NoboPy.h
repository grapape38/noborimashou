#pragma once

extern "C" {
	#include "Python.h"
}

#include "NoboTypes.h";

class CPyInstance {
public:
	CPyInstance() {
		Py_Initialize();
	}
	~CPyInstance() {
		Py_Finalize();
	}
};


class CPyObject {
private:
	PyObject* p{ nullptr };
public:
	CPyObject() = default;

	CPyObject(PyObject* _p) : p(_p) {}

	~CPyObject() {
		Release();
	}
	PyObject* getObject() {
		return p;
	}
	PyObject* setObject(PyObject* _p) {
		return (p = _p);
	}
	PyObject* AddRef() {
		if (p)
			Py_INCREF(p);
		return p;
	}
	void Release() {
		if (p)
			Py_DECREF(p);
		p = nullptr;
	}
	PyObject* operator ->() {
		return p;
	}
	bool is() {
		return p;
	}
	operator PyObject* () {
		return p;
	}
	PyObject* operator=(PyObject* pp) {
		p = pp;
		return p;
	}
	operator bool() {
		return p;
	}
};

enum class PythonMethods {
	processQuotes,
	N
};

static std::array<std::string, (size_t)PythonMethods::N> methodToStringTable = {
	"processQuotes"
};

inline const char* getMethodName(PythonMethods method) {
	return methodToStringTable[(size_t)method].c_str();
}

inline json callPythonJSON(const json& inJSON, PythonMethods method) {
	json j;

	CPyObject pName = PyUnicode_FromString("pyjson");
	CPyObject pModule = PyImport_Import(pName);

	if (pModule)
	{
		CPyObject pFunc = PyObject_GetAttrString(pModule, getMethodName(method));
		if (pFunc && PyCallable_Check(pFunc))
		{
			auto quotesJSON = PyUnicode_FromString(inJSON.dump().c_str());
			CPyObject pArgs = PyTuple_New(1);
			PyTuple_SetItem(pArgs, 0, quotesJSON);
			CPyObject pValue = PyObject_CallObject(pFunc, pArgs);
			if (!pValue) {
				throw std::runtime_error("No return value");
			}
			else {
				Py_ssize_t size;
				const char* ptr = PyUnicode_AsUTF8AndSize(pValue, &size);
				j = json::parse(std::string{ ptr, (size_t)size });
			}
		}
		else
		{
			throw std::runtime_error("Could not find function\n");
		}
	}
	else {
		throw std::runtime_error("Module not imported");
	}
	return j;
}