#include "NoboWidgets.h"
#include <QValidator>
SymbolRow::SymbolRow(int symbolID, StockUserData* stockData, QWidget* parent) :
	mInfo(&stockData->symbolInfo(symbolID))
{
	mSymbolLabel = new QSymbolLabel(
		QString::fromStdString(stockData->symName(mInfo->symbolID)));
}

QPushButton* SymbolRow::makeWatchButton(StockUserData* stockData, QWidget* parent)
{
	auto btn = new QPushButton(mInfo->isWatched ? "Unwatch" : "Watch");
	parent->connect(btn, &QPushButton::clicked,
		[id = mInfo->symbolID, stockData]() {
		stockData->toggleWatch(id);
	});
	return btn;
}

QPushButton* SymbolRow::makeHoldingButton(StockUserData* stockData, QWidget* parent)
{
	auto btn = new QPushButton(mInfo->holding ? "Remove Holding" : "Add Holding");
	parent->connect(btn, &QPushButton::clicked, [=]() {
		const auto id = mInfo->symbolID;
		if (!mInfo->holding) {
			auto dlg = new HoldingsDialog{ stockData->symName(id), parent };
			dlg->open();
			parent->connect(dlg, &HoldingsDialog::accepted, [=]() {
				stockData->addHolding(symbolID(), &dlg->getHolding());
				});
		}
		else {
			stockData->removeHolding(symbolID());
		}
	});
	return btn;
}

SymbolsList::SymbolsList(StockUserData* data, QWidget* parent) :
	mData(data), QWidget(parent) 
{
	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	setContentsMargins(10, 10, 10, 10);
	auto vBoxLayout = new QVBoxLayout(this);
	vBoxLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);

	auto filterRow = new QHBoxLayout();
	filterRow->addWidget(new QLabel("Filter: "));
	filterRow->addWidget(mFilterText = new QLineEdit);
	vBoxLayout->addLayout(filterRow);

	vBoxLayout->addLayout(mSymbolsGrid = new QGridLayout);
	
	connectSignals();
}

void SymbolsList::addSymbol(int symbolID)
{
	int j = 0;
	auto& row = addRow(symbolID)->second;
	for (auto w : row->widgets()) {
		mSymbolsGrid->addWidget(w, mSymbolRows.size() - 1, j++);
	}
}

void SymbolsList::removeSymbol(int symbolID)
{
	if (auto it = findRow(symbolID); it != rowsEnd()) {
		auto& row = it->second;
		for (auto w : row->widgets()) {
			mSymbolsGrid->removeWidget(w);
			delete w;
		}
		mSymbolRows.erase(it);
	}
}

SymbolsList::RowIterator SymbolsList::addRow(int symbolID)
{
	return mSymbolRows.insert({ symName(symbolID), makeRow(symbolID) }).first;
}

void SymbolsList::mousePressEvent(QMouseEvent*)
{
	for (const auto& r : mSymbolRows) {
		if (r.second->mSymbolLabel->underMouse()) {
			emit symbolPressed(r.second->symbolID());
		}
	}
}

void SymbolsList::filterSymbols(const QString& text)
{
	const auto hideWidgets = [](auto& r) {
		for (auto w : r->widgets()) {
			if (w->isVisible())
				w->hide();
		}
	};
	const auto showWidgets = [](auto& r) {
		for (auto w : r->widgets()) {
			if (!w->isVisible())
				w->show();
		}
	};
	if (text.isEmpty()) {
		for (auto& r : mSymbolRows) {
			showWidgets(r.second);
		}
		return;
	}
	auto tUp = text.toUpper();
	auto showIt = mSymbolRows.lower_bound(tUp);
	for (auto hideIt = mSymbolRows.begin(); hideIt != showIt; ++hideIt) {
		hideWidgets(hideIt->second);
	}
	for (; showIt != mSymbolRows.end() && showIt->first.startsWith(tUp); ++showIt) {
		showWidgets(showIt->second);
	}
	for (auto hideIt = showIt; hideIt != mSymbolRows.end(); ++hideIt) {
		hideWidgets(hideIt->second);
	}
}

void SymbolsList::connectSignals()
{
	connect(mData, &StockUserData::dataMessage, this, &SymbolsList::dataMessageDispatch);
	connect(mFilterText, &QLineEdit::textChanged, this, &SymbolsList::filterSymbols);
}

std::unique_ptr<SymbolRow> SymbolsBrowserList::makeRow(int symbolID)
{
	return std::make_unique<SymbolBrowserRow>(symbolID, mData, this);
}

void SymbolsBrowserList::dataMessageHandler(int symbolID, StockUserData::DataMessage msg)
{
	if (auto it = findRow(symbolID); it != rowsEnd())
		it->second->dataChanged(msg);
}

void SymbolBrowserRow::dataChanged(StockUserData::DataMessage msg)
{
	if (mInfo) {
		switch (msg) {
		case DataMessage::HoldingAdded:
		case DataMessage::HoldingRemoved:
			mHoldingButton->setText(
				mInfo->holding ? "Remove Holding" : "Add Holding");
			break;
		case DataMessage::WatchAdded:
		case DataMessage::WatchRemoved:
			mWatchButton->setText(
				mInfo->isWatched ? "Unwatch" : "Watch");
			break;
		}
	}
}

SymbolBrowserRow::SymbolBrowserRow(int symbolID, StockUserData * stockData, QWidget * parent) : SymbolRow(symbolID, stockData, parent)
{
	mWatchButton = makeWatchButton(stockData, parent);
	mHoldingButton = makeHoldingButton(stockData, parent);
}

std::vector<QWidget*> SymbolBrowserRow::widgets()
{
	return { mSymbolLabel, mHoldingButton, mWatchButton };
}

SymbolWatchRow::SymbolWatchRow(int symbolID, StockUserData* stockData, QWidget* parent) : SymbolRow(symbolID, stockData, parent)
{
	mWatchButton = makeWatchButton(stockData, parent);
}


void SymbolWatchRow::dataChanged(StockUserData::DataMessage msg)
{
	if (mInfo) {
		switch (msg) {
		case DataMessage::WatchAdded:
		case DataMessage::WatchRemoved:
			mWatchButton->setText(
				mInfo->isWatched ? "Unwatch" : "Watch");
		}
	}
}

std::vector<QWidget*> SymbolWatchRow::widgets()
{
	return { mSymbolLabel, mWatchButton };
}

SymbolHoldingsRow::SymbolHoldingsRow(int symbolID, StockUserData* stockData, QWidget* parent) : SymbolRow(symbolID, stockData, parent)
{
	mHoldingButton = makeHoldingButton(stockData, parent);
	auto holding = stockData->getHolding(symbolID);
	mQuantLbl = new QLabel(QString::number(holding->quantity));
	mPriceLbl = new QLabel(QString::number(holding->purchasePrice));
}

void SymbolHoldingsRow::dataChanged(StockUserData::DataMessage msg)
{
	if (mInfo) {
		switch (msg) {
		case DataMessage::HoldingAdded:
		case DataMessage::HoldingRemoved:
			mHoldingButton->setText(
				mInfo->holding ? "Remove Holding" : "Add Holding");
		}
	}
}

std::vector<QWidget*> SymbolHoldingsRow::widgets()
{
	return { mSymbolLabel, mQuantLbl, mPriceLbl, mHoldingButton };
}

std::unique_ptr<SymbolRow> SymbolsWatchList::makeRow(int symbolID)
{
	return std::make_unique<SymbolWatchRow>(symbolID, mData, this);
}

void SymbolsWatchList::dataMessageHandler(int symbolID, StockUserData::DataMessage msg)
{
	switch (msg) {
	case DataMessage::WatchAdded:
		addSymbol(symbolID);
		break;
	case DataMessage::WatchRemoved:
		removeSymbol(symbolID);
		break;
	}
}

std::unique_ptr<SymbolRow> SymbolsHoldingList::makeRow(int symbolID)
{
	return std::make_unique<SymbolHoldingsRow>(symbolID, mData, this);
}

void SymbolsHoldingList::dataMessageHandler(int symbolID, StockUserData::DataMessage msg)
{
	switch (msg) {
	case DataMessage::HoldingAdded:
		addSymbol(symbolID);
		break;
	case DataMessage::HoldingRemoved:
		removeSymbol(symbolID);
		break;
	}
}

HoldingsDialog::HoldingsDialog(const std::string& symName, QWidget* parent) : QDialog(parent), mHolding(symName)
{
	auto layout = new QGridLayout(this);
	auto quantValidator = new QIntValidator;
	auto priceValidator = new QDoubleValidator;
	layout->addWidget(new QLabel(QString::fromStdString(symName)), 0, 0);
	layout->addWidget(new QLabel("Quantity:"), 1, 0);
	layout->addWidget(mQuantityEdit = new QLineEdit(), 1, 1);
	layout->addWidget(new QLabel("Price:"), 2, 0);
	layout->addWidget(mPriceEdit = new QLineEdit(), 2, 1);
	mQuantityEdit->setValidator(quantValidator);
	mPriceEdit->setValidator(priceValidator);
	connect(mQuantityEdit, &QLineEdit::editingFinished, [this]() {
		mHolding.quantity = mQuantityEdit->text().toInt();
		});
	connect(mPriceEdit, &QLineEdit::editingFinished, [this]() {
		mHolding.purchasePrice = mPriceEdit->text().toDouble();
		});
	auto acceptButton = new QPushButton("OK");
	auto cancelButton = new QPushButton("Cancel");
	layout->addWidget(acceptButton, 3, 0);
	layout->addWidget(cancelButton, 3, 1);
	connect(acceptButton, &QPushButton::clicked, this, &QDialog::accept);
	connect(cancelButton, &QPushButton::clicked, this, &QDialog::reject);
}

QQmlEngine* getQMLEngine()
{
	static QQmlEngine engine;
	return &engine;
}
