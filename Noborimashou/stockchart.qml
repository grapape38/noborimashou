import QtQuick 2.3

Rectangle {
    width: 400
    height: 400
    property double myClose
    color: "red"
    Text {
        anchors.centerIn: parent
        text: "Close: %1".arg(myClose)
    }
}