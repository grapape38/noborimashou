extern crate chrono;
extern crate serde;
extern crate reqwest;
extern crate libc;

use std::collections::HashMap;
use std::ffi::{CStr, CString};
use libc::c_char;
use chrono::{DateTime, NaiveDate, NaiveTime, NaiveDateTime}; 
use serde::{Deserialize};
use std::error::Error;
use serde_json::Value;
use reqwest::Client;
use tokio::runtime::Runtime;

//c str ffi utilities
fn c_char_to_string<'a>(ch: *const c_char) -> String {
    unsafe { CStr::from_ptr(ch).to_str().unwrap().to_string() }
}

fn str_to_c_char(s: &str) -> *const c_char {
    CStr::from_bytes_with_nul(s.as_bytes()).unwrap().as_ptr()
}

fn u8_to_c_str(b: &[u8]) -> &CStr {
    CStr::from_bytes_with_nul(b).unwrap()
}

fn c_char_vec_to_c_str(c_vec: &[c_char]) -> &CStr {
    unsafe { CStr::from_ptr(c_vec.as_ptr()) }
}

fn string_to_c_string(s: String) -> CString {
    CString::new(s).unwrap() 
}

#[derive(Deserialize, Debug, Clone)]
pub struct DateTimeQuote {
    date: String,
    minute: String,
    open: f32,
    high: f32,
    low: f32,
    close: f32,
    volume: f32
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct Quote {
    timestamp: i64,
    open: f32,
    high: f32,
    low: f32,
    close: f32,
    volume: f32
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct QuoteList {
    quotes: *const Quote,
    n: usize
}

impl From<DateTimeQuote> for Quote {
    fn from(q: DateTimeQuote) -> Quote {
        let dflt_time = NaiveDateTime::from_timestamp(0, 0);
        let dt = NaiveDate::parse_from_str(&q.date, "%Y-%m-%d").and_then(|d| 
            NaiveTime::parse_from_str(&q.minute, "%H:%M").map(|t| d.and_time(t))
        ).unwrap_or(dflt_time);
        let timestamp = dt.timestamp();
        Quote {
            timestamp,
            open: q.open,
            high: q.high,
            low: q.low,
            close: q.close,
            volume: q.volume
        }
    }
}

#[repr(C)]
pub struct RustResponse<T, E> {
    success_cb: extern fn(T),
    error_cb: extern fn(E),
}

impl<T, E> RustResponse<T, E> {
    fn success(&self, t: T) { 
        (self.success_cb)(t)
    }
    fn err(&self, err: E) {
        (self.error_cb)(err)
    }
}

#[repr(C)]
pub enum CResult<T, E> {
    Ok(T),
    Err(E)
}

pub struct Task<T, E> {
    res: CResult<T, E>,
    start_fn: fn(&CResult<T, E>)
}

fn handle_res<T, C: CLog>(rr: RustResponse<T, *const c_char>, res: Result<T, Box<dyn Error>>, logger: &mut C) {
    match res {
        Ok(v) => { 
            rr.success(v); 
        }
        Err(e) => { 
            logger.log(&e.to_string());
            rr.err(logger.as_cstr().as_ptr());
        }
    }
}

pub trait CLog {
    fn log(&mut self, s: &str);
    fn as_cstr(&mut self) -> &CStr;
}

impl CLog for Vec<c_char> {
    fn log(&mut self, s: &str) {
        unsafe { 
            let cstring = string_to_c_string(s.to_string());
            self.extend_from_slice(std::slice::from_raw_parts(cstring.as_ptr(), s.len())) 
        }
    }
    fn as_cstr(&mut self) -> &CStr {
        if self.last().is_none() || *self.last().unwrap() != '\0' as c_char {
            self.push('\0' as c_char);
        }
        c_char_vec_to_c_str(self)
    }
}

pub const API_KEY: &'static str = "Tpk_5662078a0b824a7a88aab40b6376c7b5";

#[no_mangle]
pub extern fn get_hist_last_month(symbol: *const c_char, cbs: RustResponse<QuoteList, *const c_char>) {
    let symbol_str = c_char_to_string(symbol);
    let mut logger: Vec<c_char> = Vec::new();
    logger.log(&format!("Symbol: {}\n", symbol_str));
    //blocking
    let res = _get_hist_last_month(&symbol_str, &mut logger);
    let res_ptr = res.map(|v| QuoteList { quotes: v.as_ptr(), n: v.len() });
    handle_res(cbs, res_ptr, &mut logger);
}

const URL_BASE: &'static str = "https://sandbox.iexapis.com/stable";

//https://sandbox.iexapis.com/stable/stock/aapl/chart/1mm?token=
pub fn _get_hist_last_month(symbol: &str, logger: &mut Vec<c_char>) -> 
    Result<Vec<Quote>, Box<dyn std::error::Error>>
{
    let client = reqwest::blocking::Client::new();
    let q = &[("token", API_KEY.to_string())];

    logger.log("sending...");
    let url = format!("{}/stock/{}/chart/1mm", URL_BASE, symbol);
    let resp = client.get(&url)
        .query(q)
        .send()?;
    if !resp.status().is_success() {
        logger.log(&format!("Response code was not success: {}\n", resp.status()));
    }
    let jresp = resp.json::<Value>().map_err(|e| format!("Error getting json from response: {}\n", e))?;
    let quotes: Vec<DateTimeQuote> = serde_json::from_value(jresp).map_err(|e| format!("Error parsing quotes: {}\n", e))?;
    let c_quotes: Vec<Quote> = quotes.into_iter().map(|q| q.into()).collect();
    Ok(c_quotes)
}

#[cfg(test)]
mod test {
    use crate::_get_hist_last_month;
    use crate::CLog;
    #[test]
    pub fn test_hist_same_day() {
        let symbol = "aaplx";
        let mut logger = Vec::new();
        match _get_hist_last_month(symbol, &mut logger) {
            Ok(qs) => {
                assert!(!qs.is_empty());
                println!("retrieved {} quotes", qs.len());
            }
            Err(e) => {
                println!("{}", e);
                assert!(false);
            }
        }
        println!("{:?}", logger.as_cstr())
    }
}
