#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <new>

struct Quote {
  int64_t timestamp;
  float open;
  float high;
  float low;
  float close;
  float volume;
};

struct QuoteList {
  const Quote *quotes;
  uintptr_t n;
};

template<typename T, typename E>
struct RustResponse {
  void (*success_cb)(T);
  void (*error_cb)(E);
};

extern "C" {

void get_hist_last_month(const char *symbol, RustResponse<QuoteList, const char*> cbs);

} // extern "C"
